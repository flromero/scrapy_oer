# -- coding: utf-8 --
#print "n### Pydoc ###n"
"""
Author: Freddy Romero.
Twitter: @flromero

Arania, que permite la extraccion de datos, desde:
http://videolectures.net/

Los datos que se obtienen de cada video son los siguientes:

-Course
---title
---url
---view
---recorded
---published
---description
---author
-----name
-----url
-----university
---category
-----title
-----url

Nota: Un curso representa un video.
"""
from scrapy.spider import BaseSpider
from scrapy.selector import Selector
from scrapy.http.request import Request
import MySQLdb
import urllib2

class VideoLecturesSpider(BaseSpider):
    
    db_host = 'localhost'
    usuario = 'root'
    clave = 'root'
    base_de_datos = 'videolec'
    db = MySQLdb.connect(host=db_host, user=usuario, passwd=clave, db=base_de_datos)
    cursor = db.cursor()

    author_cont=0
    categ_cont=0
    curso_cont=0
    file_cont=0
    repo_val=0

    name = "videolec"
    #allowed_domains = ["ocw.mit.edu"]
    start_urls = [
        "http://videolectures.net/site/ajax/drilldown/?cid=1",
    ]

    def parse(self, response):
        """Recorre las 455 paginas en las cuales se encuentran los cursos"""
        for x in xrange(1,456):
            pag_url="http://videolectures.net/site/ajax/drilldown/?cid=1&p="+str(x)
            request = Request(pag_url, callback=self.parse_pagina)
            yield request
    
    def parse_pagina(self, response):
        """Metodo que recorre todos los cursos presentados en una pagina"""
        sel = Selector(response)
        cursos=sel.xpath('//a[contains(@id, "lec")]')
 
        for curso in cursos:
            url_curso="http://videolectures.net"+str(curso.xpath('@href').extract()[0])
            request = Request(url_curso, callback=self.parse_curso)
            yield request

    def parse_curso(self, response):
        """Metodo que obtiene todos los datos deseados"""
        sel = Selector(response)
        curso_r="curso"+str(self.curso_cont)
        query = """INSERT INTO datos (sujeto, predicado, objeto) VALUES(%s, %s, %s)"""
        repos="Repository1"
        if self.repo_val==0:
            name_repos="VideoLectures.NET"
            url_repos="http://videolectures.net/"
            self.cursor.execute(query,(repos, 'name',str(name_repos.encode('ascii', 'ignore'))))
            self.cursor.execute(query,(repos, 'url',str(url_repos.encode('ascii', 'ignore'))))
            self.repo_val=1

        title=sel.xpath("//title/text()").extract()[0]
        title=title.replace("VideoLectures.NET","")
        title=title.strip()
        title=title[:-1]
        title=title.strip()
        #print "TITLE: ",title
        self.cursor.execute(query,(curso_r, 'store',str(repos.encode('ascii', 'ignore'))))
        self.cursor.execute(query,(curso_r, 'title',str(title.encode('ascii', 'ignore'))))
        self.cursor.execute(query,(curso_r, 'url',str(response.url)))

        authors=sel.xpath('//div[@class="lec_data"]')
        url_authors=authors.xpath('.//a')
        texto=authors.xpath('text()').extract()
        cant=1
        for url_a in url_authors:

            author_r="author"+str(self.author_cont)
            self.cursor.execute(query,(curso_r, 'creator',str(author_r.encode('ascii', 'ignore'))))

            url_author="http://videolectures.net"+str(url_a.xpath('@href').extract()[0])
            #print "url_author: ", url_author, "\n"
            self.cursor.execute(query,(author_r, 'url',str(url_author.encode('ascii', 'ignore'))))

            name_author=url_a.xpath('text()').extract()[0];
            name_author=name_author.strip()
            aux_na=name_author.split(" ")
            firstname=aux_na[0]
            familyname=aux_na[1]
            #print "name_author: ", name_author, "\n"
            self.cursor.execute(query,(author_r, 'firstname',str(firstname.encode('ascii', 'ignore'))))
            self.cursor.execute(query,(author_r, 'familyname',str(familyname.encode('ascii', 'ignore'))))

            university=texto[cant]
            #university=university.replace(",","")
            university=university.replace("\n","")
            university=university.strip()
            #print "university: ", university, "\n"
            self.cursor.execute(query,(author_r, 'university',str(university.encode('ascii', 'ignore'))))
            cant=cant+1
            self.author_cont=self.author_cont+1

                
        aux=len(texto)   

        view= texto[aux-1].strip()
        #print "view: ", view, "\n"
        self.cursor.execute(query,(curso_r, 'view',str(view.encode('ascii', 'ignore'))))

        recorded=texto[aux-2].strip()
        #print "recorded: ", recorded, "\n"
        self.cursor.execute(query,(curso_r, 'recorded',str(recorded.encode('ascii', 'ignore'))))

        published=texto[aux-2].strip()
        #print "published: ", published, "\n"
        self.cursor.execute(query,(curso_r, 'published',str(published.encode('ascii', 'ignore'))))

        """Obtener Categorias"""
        aux2=sel.xpath('//span[@id="vl_desc"]')
        links=aux2.xpath('..//ul/li')
        for cat in links:
            categ_r="category"+str(self.categ_cont)
            aux1=len(cat.xpath("span/a"))
            #print cat
            category=cat.xpath("span/a["+str(aux1)+"]/text()").extract()[0]
            category_link="http://videolectures.net"+str(cat.xpath("span/a["+str(aux1)+"]/@href").extract()[0])

            #category=category.replace(",","")
            category=category.replace("\n","")
            category=category.strip()

            #print "CATEGORY: ",category, "\n"
            self.cursor.execute(query,(curso_r, 'category',str(categ_r.encode('ascii', 'ignore'))))
            self.cursor.execute(query,(categ_r, 'title',str(category.encode('ascii', 'ignore'))))
            self.cursor.execute(query,(categ_r, 'url',str(category_link.encode('ascii', 'ignore'))))
            self.categ_cont=self.categ_cont+1



        descriptions=sel.xpath('//div[@id="lec_desc_edit"]/p')
        description_text=""
        for description in descriptions:
            description_text=description_text+str(description.xpath("text()").extract()[0].encode('ascii', 'ignore'))
        #print "DESCRIPCION: ", description_text, "\n"

        self.cursor.execute(query,(curso_r, 'description',str(description_text.encode('ascii', 'ignore'))))


        """Obtener los archivos de video"""
        see_also1=sel.xpath('//div[@id="vl_seealso"]/p')
        if len(see_also1)>2:
            see_also=see_also1[0].xpath('a')
            for file_see in see_also:
                file_r="oer"+str(self.file_cont)
                #self.cursor.execute(query,(curso_r, 'file',str(file_r.encode('ascii', 'ignore'))))
                self.cursor.execute(query,(str(file_r.encode('ascii', 'ignore')), 'relates',curso_r))
                #file_name=file_see.xpath('text()').extract()[0]
                file_url=file_see.xpath('@href').extract()[0]

                """Obtener Meta Data del Archivo"""
                file_md=urllib2.urlopen("http://videolectures.net"+file_url)
                aux=file_md.headers
                for x in aux:
                    self.cursor.execute(query,(file_r, x.encode('ascii', 'ignore'),str(aux[x].encode('ascii', 'ignore'))))
                    #print x," ",aux[x], "\n"
                self.file_cont=self.file_cont+1   


        print "======CANTIDAD DE CURSOS: ", self.curso_cont, "======\n\n"
        
        self.curso_cont=self.curso_cont+1
        

        self.db.commit()
        

        
        

        
