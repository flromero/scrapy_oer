# -- coding: utf-8 --
from scrapy.spider import BaseSpider
from scrapy.selector import Selector
from scrapy.http.request import Request
import MySQLdb

class OcwMitSpider(BaseSpider):
    db_host = 'localhost'
    usuario = 'root'
    clave = 'root'
    base_de_datos = 'oer_mitdb'
    db = MySQLdb.connect(host=db_host, user=usuario, passwd=clave, db=base_de_datos)
    cursor = db.cursor()
    menu=0
    oer=0
    name = "ocwmit"
    #allowed_domains = ["ocw.mit.edu"]
    start_urls = [
        "http://ocw.mit.edu/courses/index.htm",
    ]

    def parse(self, response):
        #Recorrer todos los cursos
        sel = Selector(response)
        
        
        filas=sel.xpath('//table[@class="courseList"]/tbody/tr')
        #cursos=filas.xpath('td[2]/a')l
        
        
        for fila in filas:
            
            
            #f = open ("datos_cursos_V1.csv", "a")

            curso_url="http://ocw.mit.edu"+fila.xpath('td[2]/a/@href').extract()[0]

            curso_nombre=fila.xpath('td[2]/a/text()').extract()[0]
            curso_nombre=curso_nombre.replace(",", "_")
            curso_nombre=curso_nombre.replace("\n", "")
            curso_nombre=curso_nombre.replace("\t", "")
            curso_nombre=curso_nombre.strip()
            

            curso_nivel=fila.xpath('td[3]/a/text()').extract()[0]
            curso_nivel=curso_nivel.replace(",", "_")
            curso_nivel=curso_nivel.replace("\n", "")
            curso_nivel=curso_nivel.replace("\t", "")
            curso_nivel=curso_nivel.strip()

            curso=curso_nombre.replace(" ", "_")
            #datos="'"+str(curso.encode('ascii', 'ignore'))+"', 'course_url', '"+str(curso_url.encode('ascii', 'ignore'))+"'";

            query = """INSERT INTO datos (sujeto, predicado, objeto) VALUES(%s, %s, %s)"""
            self.cursor.execute(query,(str(curso.encode('ascii', 'ignore')), 'course_url',str(curso_url.encode('ascii', 'ignore'))))
            self.cursor.execute(query,(str(curso.encode('ascii', 'ignore')), 'course_title',str(curso_nombre.encode('ascii', 'ignore'))))
            self.cursor.execute(query,(str(curso.encode('ascii', 'ignore')), 'course_level',str(curso_nombre.encode('ascii', 'ignore'))))

            #f.write(str(curso.encode('ascii', 'ignore'))+","+"course_url"+","+str(curso_url.encode('ascii', 'ignore'))+"\n")
            #f.write(str(curso.encode('ascii', 'ignore'))+","+"course_title"+","+str(curso_nombre.encode('ascii', 'ignore'))+"\n")
            #f.write(str(curso.encode('ascii', 'ignore'))+","+"course_level"+","+str(curso_nombre.encode('ascii', 'ignore'))+"\n")
            
            #f.close()
            request = Request(curso_url, callback=self.parse_menus)
            yield request
        self.db.commit()
        #self.cursor.close()
        

            
    def parse_menus(self, response):
        sel = Selector(response)
        curso=sel.xpath('//title/text()').extract()[0].split("|")[0]
        curso=curso.replace(",", "_")
        curso=curso.replace("\n", "")
        curso=curso.replace("\t", "")
        curso=curso.strip()
        curso=curso.replace(" ", "_")

        curso_dep=sel.xpath('//title/text()').extract()[0].split("|")[1]
        curso_dep=curso_dep.replace(",", "_")
        curso_dep=curso_dep.replace("\n", "")
        curso_dep=curso_dep.replace("\t", "")
        curso_dep=curso_dep.strip()

        #f = open ("datos_cursos_V1.csv", "a")
        #f.write(str(curso.encode('ascii', 'ignore'))+","+"department"+","+str(curso_dep.encode('ascii', 'ignore'))+"\n")
        #f.close()
        query = """INSERT INTO datos (sujeto, predicado, objeto) VALUES(%s, %s, %s)"""
        instructores=sel.xpath("//p[@class='ins']/text()").extract()

        self.cursor.execute(query,(str(curso.encode('ascii', 'ignore')), 'department',str(curso_dep.encode('ascii', 'ignore'))))
        for instructor in instructores:
            self.cursor.execute(query,(str(curso.encode('ascii', 'ignore')), 'instructor',str(instructor.encode('ascii', 'ignore'))))

        menus=sel.xpath('//div[@id="course_nav"]/ul/li/a')
        for menu in menus:
            menu_url="http://ocw.mit.edu"+menu.xpath('@href').extract()[0]
            request = Request(menu_url, callback=self.parse_recurso)
            yield request
        self.db.commit()

    def parse_recurso(self, response):
        #f = open ("datos_cursos_V1.csv", "a")
        #print self.cursor, "CONEXION"
        #if not self.cursor.open:
        #    self.cursor = db.cursor()

        curso_menu_url=str(response.url)
        sel = Selector(response)

        curso=sel.xpath('//title/text()').extract()[0].split("|")[1]
        curso=curso.replace(",", "_")
        curso=curso.replace("\n", "")
        curso=curso.replace("\t", "")
        curso=curso.strip()
        curso=curso.replace(" ", "_")

        #Menu del curso
        curso_menu=sel.xpath('//title/text()').extract()[0].split("|")[0]
        curso_menu=curso_menu.replace(",", "_")
        curso_menu=curso_menu.strip()
        
        menu_contenido=sel.xpath('//div[@id="course_inner_section"]')
        links=menu_contenido.xpath('.//a')
        links_1=menu_contenido.xpath('.//*[a=*]')
        tabla=menu_contenido.xpath('.//table')
        query = """INSERT INTO datos (sujeto, predicado, objeto) VALUES(%s, %s, %s)"""
        

        if len(links)>0:
            menu_r="menu_"+str(self.menu)
            self.cursor.execute(query,(str(curso.encode('ascii', 'ignore')), 'menu',str(menu_r.encode('ascii', 'ignore'))))
            self.cursor.execute(query,(str(menu_r.encode('ascii', 'ignore')), 'title',str(curso_menu.encode('ascii', 'ignore'))))
            self.cursor.execute(query,(str(menu_r.encode('ascii', 'ignore')), 'url',str(curso_menu_url.encode('ascii', 'ignore'))))
            #f.write(str(curso.encode('ascii', 'ignore'))+","+"menu"+","+str(menu_r)+"\n")
            #f.write(str(menu_r)+","+"title"+","+str(curso_menu)+"\n")
            g=0
            for oer in links:
                oer_r="oer_"+str(self.oer)
                self.cursor.execute(query,(str(menu_r.encode('ascii', 'ignore')), 'oer',str(oer_r.encode('ascii', 'ignore'))))
                #f.write(str(menu_r)+","+"oer"+","+str(oer_r)+"\n")
                oer_nombre=oer.xpath('text()').extract()[0]
                if len(oer_nombre)<10:
                    oer_nombre=""
                    for x in links_1[g].xpath('text()').extract():
                        oer_nombre=oer_nombre+x
                oer_nombre=oer_nombre.replace(",", "_")
                oer_nombre=oer_nombre.replace("\n", " ")
                oer_nombre=oer_nombre.strip()
                
                oer_link="http://ocw.mit.edu"+oer.xpath('@href').extract()[0]
                self.cursor.execute(query,(str(oer_r.encode('ascii', 'ignore')), 'title',str(oer_nombre.encode('ascii', 'ignore'))))
                self.cursor.execute(query,(str(oer_r.encode('ascii', 'ignore')), 'link',str(oer_link.encode('ascii', 'ignore'))))
                
                #f.write(str(oer_r)+","+"title"+","+str(oer_nombre)+"\n")
                #f.write(str(oer_r)+","+"link"+","+str(oer_link)+"\n")
                self.oer=self.oer+1
                g=g+1
            self.menu=self.menu+1
        else:
            print "No hay OER'S en este menu: ", curso_menu, "\n"     
        self.db.commit()
        #f.close()
        #print response.url, "H"
        
