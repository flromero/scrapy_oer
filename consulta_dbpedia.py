"""
Author: Freddy Romero.
Twitter: @flromero

"""

import urllib, json
import MySQLdb, nltk


db_host = 'localhost'
usuario = 'root'
clave = 'root'
base_de_datos = 'videolec'
db = MySQLdb.connect(host=db_host, user=usuario, passwd=clave, db=base_de_datos)
cursor = db.cursor()
datos={}

def consula_dbpedia(query):
	"""Metodo que obtiene los datos de dbpedia segun el query proporcionado"""
	params={
		"default-graph": "",
		"should-sponge": "soft",
		"query": query,
		"debug": "on",
		"timeout": "",
		"format": "application/json",
		"save": "display",
		"fname": ""
	}
	querypart=urllib.urlencode(params)
	response = urllib.urlopen("http://apolo.utpl.edu.ec/vtitanio/sparql",querypart).read()
	return json.loads(response)

def cargar_datos():
	"""Obtiene los datos desde la base de datos videolec, para poder procesarlos en consultas a dbpedia"""
	query_db_mysql = ("select sujeto, objeto from datos where sujeto like '%cur%' and predicado='title' limit 2")
	cursor.execute(query_db_mysql)
	rows=cursor.fetchall()
	for title in rows:
		datos[str(title[0])]=str(title[1])
		


def procesar_datos():
	for curso in datos:
		tokens=nltk.word_tokenize(datos[curso])
		clasificar=nltk.pos_tag(tokens)
		sustantivos=[e1 for (e1, e2) in clasificar if (e2=='NN' or e2=='NNP')and len(e1)>4]
		print "NN:", sustantivos, "\n"
		for sustantivo in sustantivos:
			st="^"+sustantivo+"$"
			query_dbpedia="""PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
			select * where 
			{ ?concep skos:prefLabel ?preferLabel. 
			filter (regex(str(?preferLabel),"%s","i"))
			}""" %st

			resul_json=consula_dbpedia(query_dbpedia)
			tripletas=resul_json["results"]['bindings']
			if len(tripletas)>0:
				for tripleta in tripletas:
					print curso, "related", tripleta['concep']['value'], "\n"
					guardar_datos(curso,"related", tripleta['concep']['value'])
	db.commit()

def guardar_datos(sujeto, predicado, objeto):
	"""Metodo almacena el concepto encontrado junto con curso relacionado"""
	query = """INSERT INTO datos (sujeto, predicado, objeto) VALUES(%s, %s, %s)"""
	cursor.execute(query,(sujeto, predicado, objeto))

cargar_datos()
procesar_datos()
